
from django.core.management.commands.loaddata import Command as LoadCommand
from fast_fixtures.serializers import json_deserializer

import json

class Command(LoadCommand):
    option_list = LoadCommand.option_list + (
        make_option('--default_migrate', action='store', dest='default_migrate',
            help='Specify a default migration settings if none present.'),

    def handle(self, *args, **options):
        d = None
        if 'default_migrate' in options:
            fn = options['default_migrate']
            if not os.path.isfile(fn):
                raise IOError("Default migration file is missing: %s" % fn)
            with open(fn, 'r') as fhl:
                d = json.dumps(fhl.read())

        json_deserializer(default_migration=d)
        super(Command, self).handle(*args, **options)

